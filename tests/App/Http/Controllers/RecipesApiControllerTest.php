<?php

namespace App\Http\Controllers;

use App\Recipe;
use \Mockery as m;

class RecipesApiControllerTest extends \TestCase
{

    /**
     * Data Provider for testStoreCreated()
     * @return array
     */
    public function testStoreDataProvider()
    {
        $inputOk = [
            Recipe::BOX_TYPE => 'gourmet',
            Recipe::TITLE => 'Tenderstem and Portobello Mushrooms with Corn Polenta',
            Recipe::SLUG => 'tenderstem-and-portobello-mushrooms-with-corn-polenta',
            Recipe::SHORT_TITLE => 'This is a short title.',
            Recipe::MARKETING_DESCRIPTION => 'One for those who like their veggies with a slightly spicy kick. However, those short on time, be warned \' this is a time-consuming dish, but if you\'re willing to spend a few extra minutes in the kitchen, the fresh corn mash is extraordinary and worth a t',
            Recipe::CALORIES_KCAL => '508',
            Recipe::PROTEIN_GRAMS => '28',
            Recipe::FAT_GRAMS => '20',
            Recipe::CARBS_GRAMS => '0',
            Recipe::BULLETPOINT_1 => '',
            Recipe::BULLETPOINT_2 => '',
            Recipe::BULLETPOINT_3 => '',
            Recipe::RECIPE_DIET_TYPE_ID => 'vegetarian',
            Recipe::SEASON => 'all',
            Recipe::BASE => 'Test',
            Recipe::PROTEIN_SOURCE => 'cheese',
            Recipe::PREPARATION_TIME_MINUTES => '50',
            Recipe::SHELF_LIFE_DAYS => '4',
            Recipe::EQUIPMENT_NEEDED => 'None',
            Recipe::ORIGIN_COUNTRY => 'Great Britain',
            Recipe::RECIPE_CUISINE => 'british',
            Recipe::IN_YOUR_BOX => 'Testing',
            Recipe::GOUSTO_REFERENCE => '56',
            Recipe::RATING => '5'
        ];

        $inputBadRequest = [
            Recipe::BOX_TYPE => 'gourmet',
            Recipe::TITLE => '',
            Recipe::SLUG => 'tenderstem-and-portobello-mushrooms-with-corn-polenta',
            Recipe::SHORT_TITLE => 'This is a short title.',
            Recipe::MARKETING_DESCRIPTION => 'One for those who like their veggies with a slightly spicy kick. However, those short on time, be warned \' this is a time-consuming dish, but if you\'re willing to spend a few extra minutes in the kitchen, the fresh corn mash is extraordinary and worth a t',
            Recipe::CALORIES_KCAL => '508',
            Recipe::PROTEIN_GRAMS => '28',
            Recipe::FAT_GRAMS => '20',
            Recipe::CARBS_GRAMS => '0',
            Recipe::BULLETPOINT_1 => '',
            Recipe::BULLETPOINT_2 => '',
            Recipe::BULLETPOINT_3 => '',
            Recipe::RECIPE_DIET_TYPE_ID => 'vegetarian',
            Recipe::SEASON => 'all',
            Recipe::BASE => 'Test',
            Recipe::PROTEIN_SOURCE => 'none',
            Recipe::PREPARATION_TIME_MINUTES => '50',
            Recipe::SHELF_LIFE_DAYS => '4',
            Recipe::EQUIPMENT_NEEDED => 'None',
            Recipe::ORIGIN_COUNTRY => 'Great Britain',
            Recipe::RECIPE_CUISINE => 'british',
            Recipe::IN_YOUR_BOX => 'Testing',
            Recipe::GOUSTO_REFERENCE => '56',
            Recipe::RATING => 'NaN'
        ];

        return [
            [
                $inputOk,
                201,
                $inputOk
            ],
            [
                $inputBadRequest,
                400,
                [
                    'title' => [
                        'The title field is required.'
                    ],
                    'protein_source' => [
                        'The selected protein source is invalid.'
                    ],
                    'rating' => [
                        'The rating must be an integer.'
                    ]
                ]
            ],
        ];
    }

    /**
     * Test create new Recipe.
     *
     * @dataProvider testStoreDataProvider
     * @return void
     */
    public function testStoreCreated($input, $expectedStatusCode, $expectedContentArray)
    {
        $csvDataManipulatorMock = m::mock('App\Libraries\Gousto\Services\Csv\CsvDataManipulator')
            ->shouldAllowMockingProtectedMethods()
            ->makePartial();

        $recipesCsvRepositoryMock = m::mock('App\Libraries\Gousto\Repositories\RecipesCsvRepository[addOne,saveAll,getNextId]', [$csvDataManipulatorMock])
            ->shouldAllowMockingProtectedMethods();

        $request = new \Illuminate\Http\Request();
        $request->merge($input);

        $recipesApiController = new \App\Http\Controllers\RecipesApiController($recipesCsvRepositoryMock, $request);

        if ($expectedStatusCode == 201) {
            $recipesCsvRepositoryMock->ShouldReceive('getNextId')->once()->andReturn(1);
            $recipesCsvRepositoryMock->ShouldReceive('addOne')->once();
            $recipesCsvRepositoryMock->ShouldReceive('saveAll')->once();
        }

        $response = $recipesApiController->store($request);
        $this->assertTrue(($response instanceof \Symfony\Component\HttpFoundation\Response));
        $this->assertTrue(($response->getStatusCode() == $expectedStatusCode));
        $this->assertTrue((is_array(json_decode($response->getContent(), true))));
        $this->assertArraySubset($expectedContentArray, json_decode($response->getContent(), true));
    }
}
