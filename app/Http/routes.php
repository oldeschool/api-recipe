<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->group(
    ['prefix' => 'api/v1', 'namespace' => 'App\Http\Controllers'],
    function($group){
        $controller = 'RecipesApiController';
        $group->get('recipes', $controller . '@index');
        $group->get('recipes/search/cuisine/{recipeCuisine}', $controller . '@searchByCuisine');
        $group->post('recipes/{id}/rate', $controller . '@rate');
        $group->get('recipes/{id}', $controller . '@show');
        $group->put('recipes/{id}', $controller . '@update');
        $group->post('recipes', $controller . '@store');
        $group->delete('recipes/{id}', $controller . '@destroy');
        $group->delete('recipes', $controller . '@destroyAll');
    }
);
