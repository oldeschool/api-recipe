<?php

namespace App\Http\Controllers;

use App\Libraries\Gousto\Repositories\RecipesRepositorable;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

abstract class AbstractApiController extends Controller
{
    const PAGE     = 1;
    const PER_PAGE = 10;

    protected $pg;
    protected $perPg;

    /**
     * AbstractApiController constructor.
     *
     * @param RecipesRepositorable $recipesRepository
     */
    public function __construct(Request $request)
    {
        $pg    = $request->get('page', static::PAGE);
        $perPg = $request->get('perPage', static::PER_PAGE);

        $this->pg    = (int) ($pg) ?: static::PAGE;
        $this->perPg = (int) ($perPg) ?: static::PER_PAGE;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    abstract public function index();

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    abstract public function store(Request $request);

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    abstract public function show($id);

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    abstract public function update(Request $request, $id);

    /**
     * Remove the specified resource from storage.
     *
     * @param  $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    abstract public function destroy($id);

    /**
     * Remove all the resources from storage.
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    abstract public function destroyAll();

    /**
     * Return a paginated JSON Response.
     *
     * @param Collection $collection
     * @param int $okStatusCode
     * @param int $notFoundStatusCode
     * @return \Laravel\Lumen\Http\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function getPaginatedJsonResponse(Collection $collection, $okStatusCode = 200, $notFoundStatusCode = 404)
    {
        $resultsPaginated = $this->paginateCollection($collection);
        return ($resultsPaginated->count()) ?
            response()->json($resultsPaginated, $okStatusCode) :
            response(null, $notFoundStatusCode);
    }

    /**
     * Paginate an Eloquent Collection.
     *
     * @param Collection $collection
     * @return LengthAwarePaginator
     */
    public function paginateCollection(Collection $collection)
    {
        $offset = (int) (($this->pg - 1) * $this->perPg + 1) - 1;
        $slicedCollection = $collection->slice($offset, $this->perPg)->values();
        return new LengthAwarePaginator($slicedCollection, $collection->count(), $this->perPg, $this->pg);
    }
}
