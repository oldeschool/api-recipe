<?php

namespace App\Http\Controllers;

use App\Libraries\Gousto\Repositories\RecipesRepositorable;
use App\Recipe;
use Carbon\Carbon;
use Illuminate\Http\Request;

class RecipesApiController extends AbstractApiController
{
    protected $recipesRepository;

    /**
     * RecipesApiController constructor.
     *
     * @param RecipesRepositorable $recipesRepository
     */
    public function __construct(RecipesRepositorable $recipesRepository, Request $request)
    {
        $this->recipesRepository = $recipesRepository;

        parent::__construct($request);
    }

    /**
     * Perform a search of the resource.
     *
     * @param  string  $recipeCuisine
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function searchByCuisine($recipeCuisine)
    {
        $recipesCollection = $this->recipesRepository->fetchByCuisine($recipeCuisine);
        return $this->getPaginatedJsonResponse($recipesCollection);
    }

    /**
     * Rate a resource.
     *
     * @param  Request $request
     * @param  $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function rate(Request $request, $id)
    {
        $validator = $this->getValidationFactory()->make($request->all(), Recipe::$ratingRules);

        if ($validator->fails()) {
            return response($validator->getMessageBag(), 400);
        }

        $data = [];
        foreach (Recipe::$ratingRules as $field => $rule) {
            $data[$field] = $request->get($field);
        }
        $data[Recipe::UPDATED_AT] = Carbon::now();

        $item = $this->recipesRepository->updateOneById($id, $data);

        if ($item) {
            $this->recipesRepository->saveAll();
            return response()->json($item);
        }

        return response(null, 404);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        $recipesCollection = $this->recipesRepository->fetchAll();
        return $this->getPaginatedJsonResponse($recipesCollection);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function store(Request $request)
    {
        $validator = $this->getValidationFactory()->make($request->all(), Recipe::$createRules);

        if ($validator->fails()) {
            return response($validator->getMessageBag(), 400);
        }

        $item = new Recipe();
        $item->id         = $this->recipesRepository->getNextId(); // this normally would be auto-incremented in the DB
        $item->created_at = Carbon::now();
        $item->updated_at = Carbon::now();
        foreach (Recipe::$createRules as $field => $rule) {
            $item->{$field} = $request->get($field);
        }

        $this->recipesRepository->addOne($item);
        $this->recipesRepository->saveAll();

        return response()->json($item, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function show($id)
    {
        $item = $this->recipesRepository->fetchOneById($id);
        return ($item) ? response()->json($item) : response(null, 404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->getValidationFactory()->make($request->all(), Recipe::$updateRules);

        if ($validator->fails()) {
            return response($validator->getMessageBag(), 400);
        }

        $data = [];
        foreach (Recipe::$updateRules as $field => $rule) {
            $data[$field] = $request->get($field);
        }
        $data[Recipe::UPDATED_AT] = Carbon::now();

        $item = $this->recipesRepository->updateOneById($id, $data);

        if ($item) {
            $this->recipesRepository->saveAll();
            return response()->json($item);
        }

        return response(null, 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function destroy($id)
    {
        $item = $this->recipesRepository->removeOneById($id);
        if ($item) {
            $this->recipesRepository->saveAll();
        }

        return ($item) ? response(null, 204) : response(null, 404);
    }

    /**
     * Remove all the resources from storage.
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function destroyAll()
    {
        $count = $this->recipesRepository->removeAll();
        if ($count) {
            $this->recipesRepository->saveAll();
        }

        return ($count) ? response(null, 204) : response(null, 404);
    }
}
