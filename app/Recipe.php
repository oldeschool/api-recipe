<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recipe extends Model
{
    const ID                       = 'id';
    const CREATED_AT               = 'created_at';
    const UPDATED_AT               = 'updated_at';
    const BOX_TYPE                 = 'box_type';
    const TITLE                    = 'title';
    const SLUG                     = 'slug';
    const SHORT_TITLE              = 'short_title';
    const MARKETING_DESCRIPTION    = 'marketing_description';
    const CALORIES_KCAL            = 'calories_kcal';
    const PROTEIN_GRAMS            = 'protein_grams';
    const FAT_GRAMS                = 'fat_grams';
    const CARBS_GRAMS              = 'carbs_grams';
    const BULLETPOINT_1            = 'bulletpoint1';
    const BULLETPOINT_2            = 'bulletpoint2';
    const BULLETPOINT_3            = 'bulletpoint3';
    const RECIPE_DIET_TYPE_ID      = 'recipe_diet_type_id';
    const SEASON                   = 'season';
    const BASE                     = 'base';
    const PROTEIN_SOURCE           = 'protein_source';
    const PREPARATION_TIME_MINUTES = 'preparation_time_minutes';
    const SHELF_LIFE_DAYS          = 'shelf_life_days';
    const EQUIPMENT_NEEDED         = 'equipment_needed';
    const ORIGIN_COUNTRY           = 'origin_country';
    const RECIPE_CUISINE           = 'recipe_cuisine';
    const IN_YOUR_BOX              = 'in_your_box';
    const GOUSTO_REFERENCE         = 'gousto_reference';
    const RATING                   = 'rating';

    /**
     * The list of keys.
     *
     * @var array
     */
    public static $keys = [
        self::ID,
        self::CREATED_AT,
        self::UPDATED_AT,
        self::BOX_TYPE,
        self::TITLE,
        self::SLUG,
        self::SHORT_TITLE,
        self::MARKETING_DESCRIPTION,
        self::CALORIES_KCAL,
        self::PROTEIN_GRAMS,
        self::FAT_GRAMS,
        self::CARBS_GRAMS,
        self::BULLETPOINT_1,
        self::BULLETPOINT_2,
        self::BULLETPOINT_3,
        self::RECIPE_DIET_TYPE_ID,
        self::SEASON,
        self::BASE,
        self::PROTEIN_SOURCE,
        self::PREPARATION_TIME_MINUTES,
        self::SHELF_LIFE_DAYS,
        self::EQUIPMENT_NEEDED,
        self::ORIGIN_COUNTRY,
        self::RECIPE_CUISINE,
        self::IN_YOUR_BOX,
        self::GOUSTO_REFERENCE,
        self::RATING
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::BOX_TYPE,
        self::TITLE,
        self::SLUG,
        self::SHORT_TITLE,
        self::MARKETING_DESCRIPTION,
        self::CALORIES_KCAL,
        self::PROTEIN_GRAMS,
        self::FAT_GRAMS,
        self::CARBS_GRAMS,
        self::BULLETPOINT_1,
        self::BULLETPOINT_2,
        self::BULLETPOINT_3,
        self::RECIPE_DIET_TYPE_ID,
        self::SEASON,
        self::BASE,
        self::PROTEIN_SOURCE,
        self::PREPARATION_TIME_MINUTES,
        self::SHELF_LIFE_DAYS,
        self::EQUIPMENT_NEEDED,
        self::ORIGIN_COUNTRY,
        self::RECIPE_CUISINE,
        self::IN_YOUR_BOX,
        self::GOUSTO_REFERENCE,
        self::RATING
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The rules for validation.
     *
     * @var array
     */
    public static $createRules = [
        self::BOX_TYPE                 => 'required|in:vegetarian,gourmet',
        self::TITLE                    => 'required',
        self::SLUG                     => 'required',
        self::SHORT_TITLE              => 'required',
        self::MARKETING_DESCRIPTION    => 'required',
        self::CALORIES_KCAL            => 'required|integer',
        self::PROTEIN_GRAMS            => 'required|integer',
        self::FAT_GRAMS                => 'required|integer',
        self::CARBS_GRAMS              => 'required|integer',
        self::BULLETPOINT_1            => '',
        self::BULLETPOINT_2            => '',
        self::BULLETPOINT_3            => '',
        self::RECIPE_DIET_TYPE_ID      => 'required|in:meat,fish,vegetarian',
        self::SEASON                   => 'required|in:all',
        self::BASE                     => 'required',
        self::PROTEIN_SOURCE           => 'required|in:beef,seafood,pork,cheese,chicken,eggs,fish',
        self::PREPARATION_TIME_MINUTES => 'required|integer',
        self::SHELF_LIFE_DAYS          => 'required|integer',
        self::EQUIPMENT_NEEDED         => '',
        self::ORIGIN_COUNTRY           => 'required',
        self::RECIPE_CUISINE           => 'required|in:asian,italian,british,mediterranean,mexican',
        self::IN_YOUR_BOX              => 'required',
        self::GOUSTO_REFERENCE         => 'required|integer',
        self::RATING                   => 'integer|min:1|max:5'
    ];

    /**
     * The rules for rating validation.
     *
     * @var array
     */
    public static $ratingRules = [
        self::RATING => 'required|integer|min:1|max:5'
    ];

    /**
     * The rules for rating validation.
     *
     * @var array
     */
    public static $updateRules = [
        self::BOX_TYPE                 => 'in:vegetarian,gourmet',
        self::TITLE                    => '',
        self::SLUG                     => '',
        self::SHORT_TITLE              => '',
        self::MARKETING_DESCRIPTION    => '',
        self::CALORIES_KCAL            => 'integer',
        self::PROTEIN_GRAMS            => 'integer',
        self::FAT_GRAMS                => 'integer',
        self::CARBS_GRAMS              => 'integer',
        self::BULLETPOINT_1            => '',
        self::BULLETPOINT_2            => '',
        self::BULLETPOINT_3            => '',
        self::RECIPE_DIET_TYPE_ID      => 'in:meat,fish,vegetarian',
        self::SEASON                   => 'in:all',
        self::BASE                     => '',
        self::PROTEIN_SOURCE           => 'in:beef,seafood,pork,cheese,chicken,eggs,fish',
        self::PREPARATION_TIME_MINUTES => 'integer',
        self::SHELF_LIFE_DAYS          => 'integer',
        self::EQUIPMENT_NEEDED         => '',
        self::ORIGIN_COUNTRY           => '',
        self::RECIPE_CUISINE           => 'in:asian,italian,british,mediterranean,mexican',
        self::IN_YOUR_BOX              => '',
        self::GOUSTO_REFERENCE         => 'integer',
        self::RATING                   => 'integer|min:1|max:5'
    ];
}
