<?php

namespace App\Providers;

use App\Libraries\Gousto\Repositories\RecipesCsvRepository;
use App\Libraries\Gousto\Repositories\RecipesRepositorable;
use Laravel\Lumen\Providers\EventServiceProvider as ServiceProvider;

class RecipesRepositoryServiceProvider extends ServiceProvider
{
    public function register() {
        $this->app->bind(RecipesRepositorable::class, RecipesCsvRepository::class);
    }
}
