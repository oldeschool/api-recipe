<?php

namespace App\Libraries\Gousto\Services\Csv;

use App\Recipe;
use Illuminate\Database\Eloquent\Collection;
use League\Csv\Reader;
use League\Csv\Writer;

class CsvDataManipulator implements CsvDataManipulable
{
    /**
     * Returns an Eloquent Collection
     * from a given CSV file
     *
     * @param string $csvPath
     * @return Collection
     */
    public function getEloquentCollectionFromCsv($csvPath)
    {
        $reader  = Reader::createFromPath($csvPath);
        $results = $reader->setOffset(1)->fetchAssoc(Recipe::$keys);
        $items   = [];
        foreach ($results as $row) {
            $item = new Recipe();
            foreach ($row as $key => $value) {
                $item->{$key} = $value;
            }
            $items[] = $item;
        }
        return Collection::make($items);
    }

    /**
     * Saves an Eloquent Collection
     * to a given CSV file
     *
     * @param Collection $collection
     * @param array  $keys
     * @param string $csvPath
     * @return Collection
     */
    public function saveEloquentCollectionToCsv(Collection $collection, array $keys, $csvPath)
    {
        $csv = Writer::createFromFileObject(new \SplTempFileObject());
        $csv->insertOne($keys);
        $csv->insertAll($collection->toArray());
        file_put_contents($csvPath, $csv->__toString());
    }
}