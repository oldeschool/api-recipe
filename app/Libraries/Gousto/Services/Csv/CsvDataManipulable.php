<?php

namespace App\Libraries\Gousto\Services\Csv;

use Illuminate\Database\Eloquent\Collection;

interface CsvDataManipulable
{
    public function getEloquentCollectionFromCsv($csvPath);
    public function saveEloquentCollectionToCsv(Collection $collection, array $keys, $csvPath);
}