<?php

namespace App\Libraries\Gousto\Repositories;

use App\Libraries\Gousto\Services\Csv\CsvDataManipulator;
use App\Recipe;
use Illuminate\Database\Eloquent\Collection;

class RecipesCsvRepository implements RecipesRepositorable
{
    const RECIPES_CSV = '/recipes.csv';

    protected $csvService;
    protected $collection;

    /**
     * Create a new RecipesRepository instance.
     *
     * @return void
     */
    public function __construct(CsvDataManipulator $csvService)
    {
        $this->csvService = $csvService;
        $this->collection = $this->csvService->getEloquentCollectionFromCsv(database_path() . static::RECIPES_CSV);
    }

    /**
     * Returns an Eloquent Collection
     * of Recipes by cuisine
     *
     * @param string $recipeCuisine
     * @return Collection
     */
    public function fetchByCuisine($recipeCuisine)
    {
        return $this->collection->where(Recipe::RECIPE_CUISINE, $recipeCuisine);
    }

    /**
     * Returns an Eloquent Collection
     * of all Recipes
     *
     * @return Collection
     */
    public function fetchAll()
    {
        return $this->collection;
    }

    /**
     * Returns one Recipe
     *
     * @param int $id
     * @return Recipe
     */
    public function fetchOneById($id)
    {
        return $this->collection->find($id);
    }

    /**
     * Updates one Recipe
     * in the Collection
     *
     * @param $id
     * @param array $data
     * @return Recipe|false
     */
    public function updateOneById($id, array $data)
    {
        $item = $this->fetchOneById($id);

        if ($item) {
            foreach ($data as $key => $value) {
                $item->{$key} = $value;
            }
            return $item;
        }
        return false;
    }

    /**
     * Removes one Recipe
     * from the Collection
     *
     * @param $id
     * @return Recipe|false
     */
    public function removeOneById($id)
    {
        $item = $this->fetchOneById($id);

        if ($item) {
            $this->collection = $this->collection->filter(function ($item) use ($id) {
                return ($item->{Recipe::ID} != $id);
            });
            return $item;
        }
        return false;
    }


    /**
     * Removes all Recipes
     * from the Collection
     *
     * @return bool
     */
    public function removeAll()
    {
        $count = $this->collection->count();
        if ($count > 0) {
            $this->collection = $this->collection->take(0);
        }
        return $count;
    }

    /**
     * Adds one Recipe
     * to Collection
     *
     * @param Recipe $item
     * @return Recipe
     */
    public function addOne(Recipe $item)
    {
        $this->collection->add($item);
        return $item;
    }

    /**
     * Saves the Collection
     * to Csv
     *
     * @return Recipe
     */
    public function saveAll()
    {
        return $this->csvService->saveEloquentCollectionToCsv($this->collection, Recipe::$keys, database_path() . static::RECIPES_CSV);
    }

    /**
     * Returns the next available id
     * in the Csv database
     *
     * @param $idField
     * @return integer
     */
    public function getNextId($idField = 'id')
    {
        $maxId = $this->collection->sortBy($idField)
            ->last()->{$idField};
        return $maxId + 1;
    }

}