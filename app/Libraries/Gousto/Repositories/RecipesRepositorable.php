<?php

namespace App\Libraries\Gousto\Repositories;

use App\Recipe;

interface RecipesRepositorable
{
    public function fetchByCuisine($recipeCuisine);
    public function fetchAll();
    public function fetchOneById($id);
    public function updateOneById($id, array $data);
    public function removeOneById($id);
    public function removeAll();
    public function addOne(Recipe $item);
    public function saveAll();
    public function getNextId();
}