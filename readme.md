# Recipe API

A simple yet effective RESTful Recipe API.

## Installation

- Clone the repository
    - `git clone https://GIG0i@bitbucket.org/oldeschool/api-recipe.git`
- Run composer update
    - `cd api-recipe`
    - `php composer.phar update`
- Create `.env` configuration file

```
APP_ENV=local
APP_DEBUG=true
APP_KEY=RandomGeneratedKey
```

**Please note**: *If you experience errors such as 500 Internal Server Error when hitting the API after installation, it's likely you need to check the folder and files permissions. Please check the system logs to help identify the issue.*

## Usage

Postman (https://www.getpostman.com/docs) collection available at: https://bitbucket.org/oldeschool/api-recipe/src/master/api-recipe.json.postman_collection

Live version is testable at: http://api.recipe.oldschoolcloud.com

#### Available routes/examples:

- List all recipes: GET http://api.recipe.oldschoolcloud.com/api/v1/recipes?page=1&perPage=5
- Show a recipe: GET http://api.recipe.oldschoolcloud.com/api/v1/recipes/4
- Search recipes by cuisine: GET http://api.recipe.oldschoolcloud.com/api/v1/recipes/search/cuisine/italian?page=1&perPage=1
- \* Rate a recipe: POST http://api.recipe.oldschoolcloud.com/api/v1/recipes/4/rate
- \* Update a recipe: PUT http://api.recipe.oldschoolcloud.com/api/v1/recipes/4
- \* Store a recipe: POST http://api.recipe.oldschoolcloud.com/api/v1/recipes
- Destroy a recipe: DELETE http://api.recipe.oldschoolcloud.com/api/v1/recipes/4
- Destroy all recipes: DELETE http://api.recipe.oldschoolcloud.com/api/v1/recipes

*\* Parameters are required, please check the Postman collection for the complete list.*
## Why Laravel Lumen?

*"Laravel Lumen is a stunningly fast PHP micro-framework for building web applications with expressive, elegant syntax."*

The main reasons I decided to use Laravel Lumen for this particular API are:

- Extremely lightweight and fast
    - Since Lumen is a trimmed down version of Laravel, you get most of Laravel's functionality but with more speed. 
- Easy to implement
    - A lot of tasks such as routing, queueing, caching, authentication are made extremely easy to implement. This allows developers of all levels to work and integrate without having to go through a steep learning curve.
- Good community and documentation
    - Laravel frameworks are now widely used and it's very likely to find support, examples and packages online. The official documentation is also very good and gives a full view of each single piece of functionality. Again this translates into an easier approach from developers of all levels.

## A solution for web and mobile

Being a RESTful service and making use of a cross platform compatible JSON data structure, this API is easily consumable by both web and mobile applications.

## Unit Testing

**Please note**: *For demonstration purposes unit testing has been limited to one method*

- Run unit tests
    - `phpunit` or `vendor/phpunit/phpunit/phpunit`

#### Expected results:

```
PHPUnit 4.8.24 by Sebastian Bergmann and contributors.

...

Time: 922 ms, Memory: 7.00Mb

OK (3 tests, 8 assertions)
```

## Notes

The following has not been implemented because not required but strongly recommended:

- Authentication for Rate, Update, Store, Destroy, DestroyAll routes
- Results or search query caching
- Complete Unit Testing code coverage
- Use of database such as MySQL or similar and creation of schema migrations and seeds
- Implementation of soft-delete for Recipes